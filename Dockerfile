FROM openjdk:8 as build

# Work around a bug in Java 1.8u181 / the Maven Surefire plugin.
# See https://stackoverflow.com/questions/53010200 and
# https://issues.apache.org/jira/browse/SUREFIRE-1588.
ENV JAVA_TOOL_OPTIONS "-Djdk.net.URLClassPath.disableClassPathURLCheck=true"

ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH

# Tomcat folder creation inside Docker container
# Download and install Apache Tomcat.
RUN mkdir -p /opt/tomcat
RUN curl "https://www-eu.apache.org/dist/tomcat/tomcat-8/v8.5.47/bin/apache-tomcat-8.5.47.tar.gz" > /opt/tomcat/tomcat.tar.gz
RUN tar -C /opt/tomcat -xf /opt/tomcat/tomcat.tar.gz --strip-components 1
ENV CATALINA_HOME /opt/tomcat

ENV ANT_HOME /usr/local/ant
ENV PATH ${PATH}:/usr/local/ant/bin

# Added ANT tool from host to docker container
ADD apache-ant-1.9 /usr/local/ant

# Added application from host to docker container
# TempProject contains build.xml , src and webroot
ADD opus /usr/local/opus  

WORKDIR /usr/local/opus/opus/applications/default_all/

RUN ant war

COPY postgresql-9.2-1004.jdbc41.jar /opt/tomcat/lib

FROM openjdk:8

COPY --from=build /opt/tomcat /opt/tomcat
COPY --from=build /usr/local/opus/opus/dist/opus.war /opt/tomcat/webapps/virtu.war
COPY context.xml /opt/tomcat/conf/

EXPOSE 8080
WORKDIR /opt/tomcat/bin
CMD ./startup.sh && tail -f ../logs/catalina.out
